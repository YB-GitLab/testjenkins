FROM jenkins/jenkins:lts-jdk11
USER root
RUN apt-get update && apt-get install -y maven
# drop back to the regular jenkins user - good practice
USER jenkins
ENV JENKINS_OPTS --argumentsRealm.roles.user=admin --argumentsRealm.passwd.admin=admin --argumentsRealm.roles.admin=admin
ENV JENKINS_SLAVE_AGENT_PORT 50001
EXPOSE 8080
